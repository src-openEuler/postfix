%undefine _strict_symbol_defs_build

%{!?_hardened_build:%global _hardened_build 1}

%define postfix_config_dir      %{_sysconfdir}/postfix
%define postfix_daemon_dir      %{_libexecdir}/postfix
%define postfix_shlib_dir       %{_libdir}/postfix
%define postfix_command_dir     %{_sbindir}
%define postfix_queue_dir       %{_var}/spool/postfix
%define postfix_data_dir        %{_var}/lib/postfix
%define postfix_doc_dir         %{?_pkgdocdir}%{!?_pkgdocdir:%{_docdir}/%{name}-%{version}}
%define postfix_sample_dir      %{postfix_doc_dir}/samples
%define postfix_readme_dir      %{postfix_doc_dir}/README_FILES

%global sslcert %{_sysconfdir}/pki/tls/certs/postfix.pem
%global sslkey  %{_sysconfdir}/pki/tls/private/postfix.key

%global _privatelibs libpostfix-.+\.so.*
%global __provides_exclude ^(%{_privatelibs})$
%global __requires_exclude ^(%{_privatelibs})$
%global sasl_config_dir %{_sysconfdir}/sasl2

Name:           postfix
Summary:        Postfix Mail Transport Agent
Version:        3.8.4
Release:        1
Epoch:          2
URL:            http://www.postfix.org
License:        (IPL-1.0 and GPLv2+) or (EPL-2.0 and GPLv2+)
Source0:        http://ftp.porcupine.org/mirrors/postfix-release/official/%{name}-%{version}.tar.gz
Source1:        postfix-etc-init.d-postfix
Source2:        postfix.service
Source3:        postfix.aliasesdb
Source4:        postfix-chroot-update
Source53:       https://jimsun.linxnet.com/downloads/pflogsumm-1.1.5.tar.gz
Source100:      postfix-sasl.conf
Source101:      postfix-pam.conf

Patch1:         postfix-3.2.0-config.patch
Patch2:         postfix-3.1.0-files.patch
Patch3:         postfix-3.1.0-alternatives.patch
Patch4:         postfix-3.2.0-large-fs.patch
Patch5:         pflogsumm-1.1.5-datecalc.patch
Patch6:         pflogsumm-1.1.5-ipv6-warnings-fix.patch 
Patch11:        postfix-use-lmdb-by-default-instead-of-libdb.patch
Patch12:        postfix-set-default_data_type-to-lmdb.patch

BuildRequires:  lmdb-devel perl-generators pkgconfig zlib-devel systemd-units libicu-devel libnsl2-devel gcc m4 openldap-devel
BuildRequires:  cyrus-sasl-devel pcre2-devel mariadb-connector-c-devel postgresql-devel sqlite-devel openssl-devel procps-ng
BuildRequires:  chrpath findutils
Requires(post): systemd-sysv %{_sbindir}/alternatives %{_bindir}/openssl
Requires(pre):  %{_sbindir}/groupadd %{_sbindir}/useradd
Requires(preun): %{_sbindir}/alternatives
%{?systemd_requires}
Requires: diffutils findutils %{name}-help

Provides:       MTA smtpd smtpdaemon server(smtp)
Provides:       %{name}-mysql %{name}-sqlite %{name}-ldap %{name}-pcre
Obsoletes:      %{name}-mysql %{name}-sqlite %{name}-ldap %{name}-pcre

%description
Postfix is a Mail Transport Agent (MTA).

%package        sysvinit
Summary:        SysV initscript for postfix
BuildArch:      noarch
Requires:       %{name} = %{epoch}:%{version}-%{release}
Requires(preun): chkconfig
Requires(post): chkconfig

%description    sysvinit
This package contains the SysV initscript.

%package        perl-scripts
Summary:        Postfix utilities written in perl
Requires:       %{name} = %{epoch}:%{version}-%{release}
Obsoletes:      postfix < 2:2.5.5-2
Provides:       postfix-pflogsumm = %{epoch}:%{version}-%{release}
Obsoletes:      postfix-pflogsumm < 2:2.5.5-2

%description    perl-scripts
This package contains perl scripts pflogsumm and qshape.
Pflogsumm is a log analyzer/summarizer for the Postfix MTA. It is
designed to provide an over-view of Postfix activity. Pflogsumm
generates summaries and, in some cases, detailed reports of mail
server traffic volumes, rejected and bounced email, and server
warnings, errors and panics.
qshape prints Postfix queue domain and age distribution.

%package        pgsql
Summary:        Postfix PostgreSQL map support
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description    pgsql
This provides support for PostgreSQL  maps in Postfix. If you plan to use
PostgreSQL maps with Postfix, you need this.

%package_help

%prep
%setup -qn %{name}-%{version}
%patch1 -p1 -b .config
%patch2 -p1 -b .files
%patch3 -p1 -b .alternatives
%patch4 -p1 -b .large-fs

%patch11 -p1
%patch12 -p1

sed -i \
's|^\(\s*#define\s\+DEF_SHLIB_DIR\s\+\)"/usr/lib/postfix"|\1"%{_libdir}/postfix"|' \
src/global/mail_params.h
sed -i -e "s/DB_VERSION_MAJOR == 6/DB_VERSION_MAJOR >= 6/" \
src/util/dict_db.c

gzip -dc %{SOURCE53} | tar xf -
pushd pflogsumm-1.1.5
%patch5 -p1 -b .datecalc
%patch6 -p1 -b .ipv6-warnings-fix
popd

sed -i makedefs -e '\@Linux\.@s|345|3456|'
sed -i src/util/sys_defs.h -e 's@defined(LINUX5)@defined(LINUX5) || defined(LINUX6)@'

for f in README_FILES/TLS_{LEGACY_,}README TLS_ACKNOWLEDGEMENTS; do
        iconv -f iso8859-1 -t utf8 -o ${f}{_,} &&
                touch -r ${f}{,_} && mv -f ${f}{_,}
done

%build
unset AUXLIBS AUXLIBS_LDAP AUXLIBS_LMDB AUXLIBS_PCRE AUXLIBS_MYSQL AUXLIBS_PGSQL AUXLIBS_SQLITE AUXLIBS_CDB
CCARGS="-fPIC"
AUXLIBS="-lnsl"
CCARGS="${CCARGS} -DHAS_LDAP -DLDAP_DEPRECATED=1 %{?with_sasl:-DUSE_LDAP_SASL}"
AUXLIBS_LDAP="-lldap -llber"
CCARGS="${CCARGS} -DHAS_LMDB"
AUXLIBS_LMDB="-llmdb"
CCARGS="${CCARGS} -DHAS_PCRE=2 `pcre2-config --cflags`"
AUXLIBS_PCRE="`pcre2-config --libs8`"
CCARGS="${CCARGS} -DHAS_MYSQL -I%{_includedir}/mysql"
AUXLIBS_MYSQL="-L%{_libdir}/mariadb -lmysqlclient -lm"
CCARGS="${CCARGS} -DHAS_PGSQL -I%{_includedir}/pgsql"
AUXLIBS_PGSQL="-lpq"
CCARGS="${CCARGS} -DHAS_SQLITE `pkg-config --cflags sqlite3`"
AUXLIBS_SQLITE="`pkg-config --libs sqlite3`"
CCARGS="${CCARGS} -DUSE_SASL_AUTH -DUSE_CYRUS_SASL -I%{_includedir}/sasl"
AUXLIBS="${AUXLIBS} -L%{_libdir}/sasl2 -lsasl2"

if pkg-config openssl ; then
    CCARGS="${CCARGS} -DUSE_TLS `pkg-config --cflags openssl`"
    AUXLIBS="${AUXLIBS} `pkg-config --libs openssl`"
else
    CCARGS="${CCARGS} -DUSE_TLS -I/usr/include/openssl"
    AUXLIBS="${AUXLIBS} -lssl -lcrypto"
fi

CCARGS="${CCARGS} -DDEF_CONFIG_DIR=\\\"%{postfix_config_dir}\\\""
CCARGS="${CCARGS} $(getconf LFS_CFLAGS)"

CCARGS="${CCARGS} -DNO_DB" 
LDFLAGS="%{?__global_ldflags} %{?_hardened_build:-Wl,-z,relro,-z,now}"

make -f Makefile.init makefiles shared=yes dynamicmaps=yes \
  %{?_hardened_build:pie=yes} CCARGS="${CCARGS}" AUXLIBS="${AUXLIBS}" \
  AUXLIBS_LDAP="${AUXLIBS_LDAP}" AUXLIBS_LMDB="${AUXLIBS_LMDB}" AUXLIBS_PCRE="${AUXLIBS_PCRE}" \
  AUXLIBS_MYSQL="${AUXLIBS_MYSQL}" AUXLIBS_PGSQL="${AUXLIBS_PGSQL}" \
  AUXLIBS_SQLITE="${AUXLIBS_SQLITE}" AUXLIBS_CDB="${AUXLIBS_CDB}"\
  DEBUG="" SHLIB_RPATH="-Wl,-rpath,%{postfix_shlib_dir} $LDFLAGS" \
  OPT="$RPM_OPT_FLAGS -fno-strict-aliasing -Wno-comment" \
  POSTFIX_INSTALL_OPTS=-keep-build-mtime

%make_build

%install
mkdir -p %{buildroot}

for i in man1/mailq.1 man1/newaliases.1 man1/sendmail.1 man5/aliases.5 man8/smtpd.8; do
  dest=$(echo $i | sed 's|\.[1-9]$|.postfix\0|')
  mv man/$i man/$dest
  sed -i "s|^\.so $i|\.so $dest|" man/man?/*.[1-9]
done

make non-interactive-package \
       install_root=%{buildroot} \
       config_directory=%{postfix_config_dir} \
       meta_directory=%{postfix_config_dir} \
       shlib_directory=%{postfix_shlib_dir} \
       daemon_directory=%{postfix_daemon_dir} \
       command_directory=%{postfix_command_dir} \
       queue_directory=%{postfix_queue_dir} \
       data_directory=%{postfix_data_dir} \
       sendmail_path=%{postfix_command_dir}/sendmail.postfix \
       newaliases_path=%{_bindir}/newaliases.postfix \
       mailq_path=%{_bindir}/mailq.postfix \
       mail_owner=postfix \
       setgid_group=postdrop \
       manpage_directory=%{_mandir} \
       sample_directory=%{postfix_sample_dir} \
       readme_directory=%{postfix_readme_dir} || exit 1

mkdir -p %{buildroot}%{_initrddir}
install -c %{SOURCE1} %{buildroot}%{_initrddir}/postfix

mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE2} %{buildroot}%{_unitdir}
install -m 755 %{SOURCE3} %{buildroot}%{postfix_daemon_dir}/aliasesdb
install -m 755 %{SOURCE4} %{buildroot}%{postfix_daemon_dir}/chroot-update

install -c auxiliary/rmail/rmail %{buildroot}%{_bindir}/rmail.postfix

for i in active bounce corrupt defer deferred flush incoming private saved maildrop public pid saved trace; do
    mkdir -p %{buildroot}%{postfix_queue_dir}/$i
done

for i in smtp-sink smtp-source ; do
  install -c -m 755 bin/$i %{buildroot}%{postfix_command_dir}/
  install -c -m 755 man/man1/$i.1 %{buildroot}%{_mandir}/man1/
done

sed -i -r "s#(/man[158]/.*.[158]):f#\1.gz:f#" %{buildroot}%{postfix_config_dir}/postfix-files

cat %{buildroot}%{postfix_config_dir}/postfix-files

mkdir -p %{buildroot}%{sasl_config_dir}
install -m 644 %{SOURCE100} %{buildroot}%{sasl_config_dir}/smtpd.conf

mkdir -p %{buildroot}%{_sysconfdir}/pam.d
install -m 644 %{SOURCE101} %{buildroot}%{_sysconfdir}/pam.d/smtp.postfix

mkdir -p %{buildroot}%{postfix_doc_dir}
cp -p COMPATIBILITY LICENSE TLS_ACKNOWLEDGEMENTS TLS_LICENSE %{buildroot}%{postfix_doc_dir}

mkdir -p %{buildroot}%{postfix_doc_dir}/examples{,/chroot-setup}
cp -pr examples/{qmail-local,smtpd-policy} %{buildroot}%{postfix_doc_dir}/examples
cp -p examples/chroot-setup/LINUX2 %{buildroot}%{postfix_doc_dir}/examples/chroot-setup

cp conf/{main,bounce}.cf.default %{buildroot}%{postfix_doc_dir}
sed -i 's#%{postfix_config_dir}\(/bounce\.cf\.default\)#%{postfix_doc_dir}\1#' %{buildroot}%{_mandir}/man5/bounce.5
rm -f %{buildroot}%{postfix_config_dir}/{TLS_,}LICENSE

find %{buildroot}%{postfix_doc_dir} -type f | xargs chmod 644
find %{buildroot}%{postfix_doc_dir} -type d | xargs chmod 755

install -c -m 644 pflogsumm-1.1.5/pflogsumm-faq.txt %{buildroot}%{postfix_doc_dir}/pflogsumm-faq.txt
install -c -m 644 pflogsumm-1.1.5/pflogsumm.1 %{buildroot}%{_mandir}/man1/pflogsumm.1
install -c pflogsumm-1.1.5/pflogsumm.pl %{buildroot}%{postfix_command_dir}/pflogsumm

mantools/srctoman - auxiliary/qshape/qshape.pl > qshape.1
install -c qshape.1 %{buildroot}%{_mandir}/man1/qshape.1
install -c auxiliary/qshape/qshape.pl %{buildroot}%{postfix_command_dir}/qshape

rm -f %{buildroot}%{postfix_config_dir}/aliases

mkdir -p %{buildroot}/usr/lib
pushd %{buildroot}/usr/lib
ln -sf ../sbin/sendmail.postfix .
popd

mkdir -p %{buildroot}%{_var}/lib/misc
touch %{buildroot}%{_var}/lib/misc/postfix.aliasesdb-stamp

for i in %{postfix_command_dir}/sendmail %{_bindir}/{mailq,newaliases,rmail} \
        %{_sysconfdir}/pam.d/smtp /usr/lib/sendmail \
        %{_mandir}/{man1/{mailq.1,newaliases.1},man5/aliases.5,man8/{sendmail.8,smtpd.8}}
do
        touch %{buildroot}$i
done

chrpath -d %{buildroot}%{_libexecdir}/postfix/anvil
chrpath -d %{buildroot}%{_libexecdir}/postfix/postlogd
chrpath -d %{buildroot}%{_libexecdir}/postfix/spawn
chrpath -d %{buildroot}%{_libexecdir}/postfix/trivial-rewrite
chrpath -d %{buildroot}%{_sbindir}/postlock
chrpath -d %{buildroot}%{_sbindir}/postkick
chrpath -d %{buildroot}%{_sbindir}/postfix
chrpath -d %{buildroot}%{_sbindir}/postlog
chrpath -d %{buildroot}%{_sbindir}/postdrop
chrpath -d %{buildroot}%{_libdir}/postfix/*.so*
chrpath -d %{buildroot}%{_libexecdir}/postfix/smtp
chrpath -d %{buildroot}%{_libexecdir}/postfix/pipe
chrpath -d %{buildroot}%{_libexecdir}/postfix/master
chrpath -d %{buildroot}%{_libexecdir}/postfix/nqmgr
chrpath -d %{buildroot}%{_libexecdir}/postfix/scache
chrpath -d %{buildroot}%{_libexecdir}/postfix/cleanup
chrpath -d %{buildroot}%{_libexecdir}/postfix/postscreen
chrpath -d %{buildroot}%{_libexecdir}/postfix/error
chrpath -d %{buildroot}%{_libexecdir}/postfix/proxymap
chrpath -d %{buildroot}%{_libexecdir}/postfix/showq
chrpath -d %{buildroot}%{_libexecdir}/postfix/qmgr
chrpath -d %{buildroot}%{_libexecdir}/postfix/dnsblog
chrpath -d %{buildroot}%{_libexecdir}/postfix/flush
chrpath -d %{buildroot}%{_libexecdir}/postfix/local
chrpath -d %{buildroot}%{_libexecdir}/postfix/verify
chrpath -d %{buildroot}%{_libexecdir}/postfix/smtpd
chrpath -d %{buildroot}%{_libexecdir}/postfix/qmqpd
chrpath -d %{buildroot}%{_libexecdir}/postfix/virtual
chrpath -d %{buildroot}%{_libexecdir}/postfix/lmtp
chrpath -d %{buildroot}%{_libexecdir}/postfix/bounce
chrpath -d %{buildroot}%{_libexecdir}/postfix/tlsmgr
chrpath -d %{buildroot}%{_libexecdir}/postfix/oqmgr
chrpath -d %{buildroot}%{_libexecdir}/postfix/pickup
chrpath -d %{buildroot}%{_libexecdir}/postfix/discard
chrpath -d %{buildroot}%{_libexecdir}/postfix/tlsproxy
chrpath -d %{buildroot}%{_sbindir}/sendmail.postfix
chrpath -d %{buildroot}%{_sbindir}/smtp-source
chrpath -d %{buildroot}%{_sbindir}/postmap
chrpath -d %{buildroot}%{_sbindir}/postalias
chrpath -d %{buildroot}%{_sbindir}/postqueue
chrpath -d %{buildroot}%{_sbindir}/postcat
chrpath -d %{buildroot}%{_sbindir}/smtp-sink
chrpath -d %{buildroot}%{_sbindir}/postconf
chrpath -d %{buildroot}%{_sbindir}/postsuper
chrpath -d %{buildroot}%{_sbindir}/postmulti

mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/postfix" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

function split_file
{
  grep "$1" "$3" >> "$3.d/$2" || :
  sed -i "\|$1| d" "$3" || :
}

pushd %{buildroot}%{postfix_config_dir}
for map in mysql pgsql sqlite ldap lmdb pcre; do
  rm -f dynamicmaps.cf.d/"$map" "postfix-files.d/$map"
  split_file "^\s*$map\b" "$map" dynamicmaps.cf
  sed -i "s|postfix-$map\\.so|%{postfix_shlib_dir}/\\0|" "dynamicmaps.cf.d/$map"
  split_file "^\$shlib_directory/postfix-$map\\.so:" "$map" postfix-files
  split_file "^\$manpage_directory/man5/${map}_table\\.5" "$map" postfix-files
  map_upper=`echo $map | tr '[:lower:]' '[:upper:]'`
  split_file "^\$readme_directory/${map_upper}_README:" "$map" postfix-files
done
popd

%post -e
%systemd_post %{name}.service
/sbin/ldconfig

%{_sbindir}/postfix set-permissions upgrade-configuration \
        daemon_directory=%{postfix_daemon_dir} \
        command_directory=%{postfix_command_dir} \
        mail_owner=postfix \
        setgid_group=postdrop \
        manpage_directory=%{_mandir} \
        sample_directory=%{postfix_sample_dir} \
        readme_directory=%{postfix_readme_dir} &> /dev/null

ALTERNATIVES_DOCS=""
[ "%%{_excludedocs}" = 1 ] || ALTERNATIVES_DOCS='--slave %{_mandir}/man1/mailq.1.gz mta-mailqman %{_mandir}/man1/mailq.postfix.1.gz
        --slave %{_mandir}/man1/newaliases.1.gz mta-newaliasesman %{_mandir}/man1/newaliases.postfix.1.gz
        --slave %{_mandir}/man5/aliases.5.gz mta-aliasesman %{_mandir}/man5/aliases.postfix.5.gz
        --slave %{_mandir}/man8/sendmail.8.gz mta-sendmailman %{_mandir}/man1/sendmail.postfix.1.gz
        --slave %{_mandir}/man8/smtpd.8.gz mta-smtpdman %{_mandir}/man8/smtpd.postfix.8.gz'

%{_sbindir}/alternatives --install %{postfix_command_dir}/sendmail mta %{postfix_command_dir}/sendmail.postfix 60 \
        --slave %{_bindir}/mailq mta-mailq %{_bindir}/mailq.postfix \
        --slave %{_bindir}/newaliases mta-newaliases %{_bindir}/newaliases.postfix \
        --slave %{_sysconfdir}/pam.d/smtp mta-pam %{_sysconfdir}/pam.d/smtp.postfix \
        --slave %{_bindir}/rmail mta-rmail %{_bindir}/rmail.postfix \
        --slave /usr/lib/sendmail mta-sendmail /usr/lib/sendmail.postfix \
        $ALTERNATIVES_DOCS \
        --initscript postfix

if [ -f %{_libdir}/sasl2/smtpd.conf ]; then
        mv -f %{_libdir}/sasl2/smtpd.conf %{sasl_config_dir}/smtpd.conf
        /sbin/restorecon %{sasl_config_dir}/smtpd.conf 2> /dev/null
fi

if [ ! -f %{sslkey} ]; then
  umask 077
  %{_bindir}/openssl genrsa 4096 > %{sslkey} 2> /dev/null
fi

if [ ! -f %{sslcert} ]; then
  if [ -x %{_bindir}/hostname ] ; then
    FQDN=`hostname`
    if [ "x${FQDN}" = "x" ]; then
      FQDN=localhost.localdomain
    fi
  else
    FQDN=localhost.localdomain
  fi

  %{_bindir}/openssl req -new -key %{sslkey} -x509 -sha256 -days 730 -set_serial $RANDOM -out %{sslcert} \
    -subj "/C=--/ST=SomeState/L=SomeCity/O=SomeOrganization/OU=SomeOrganizationalUnit/CN=${FQDN}/emailAddress=root@${FQDN}"
  chmod 644 %{sslcert}
fi

exit 0

%pre
%{_sbindir}/groupadd -g 90 -r postdrop 2>/dev/null
%{_sbindir}/groupadd -g 89 -r postfix 2>/dev/null
%{_sbindir}/groupadd -g 12 -r mail 2>/dev/null
%{_sbindir}/useradd -d %{postfix_queue_dir} -s /sbin/nologin -g postfix -G mail -M -r -u 89 postfix 2>/dev/null

if [ -e %{_mandir}/man8/smtpd.8.gz ]; then
        [ -h %{_mandir}/man8/smtpd.8.gz ] || rm -f %{_mandir}/man8/smtpd.8.gz
fi

exit 0

%preun
%systemd_preun %{name}.service

if [ "$1" = 0 ]; then
    %{_sbindir}/alternatives --remove mta %{postfix_command_dir}/sendmail.postfix
fi
exit 0

%postun
%systemd_postun_with_restart %{name}.service
/sbin/ldconfig

%post sysvinit
/sbin/chkconfig --add postfix >/dev/null 2>&1 ||:

%preun sysvinit
if [ "$1" = 0 ]; then
    %{_initrddir}/postfix stop >/dev/null 2>&1 ||:
    /sbin/chkconfig --del postfix >/dev/null 2>&1 ||:
fi

%postun sysvinit
[ "$1" -ge 1 ] && %{_initrddir}/postfix condrestart >/dev/null 2>&1 ||:

%triggerpostun -n postfix-sysvinit -- postfix < 2.8.12-2
/sbin/chkconfig --add postfix >/dev/null 2>&1 || :

%triggerun -- postfix < 2.8.12-2
%{_bindir}/systemd-sysv-convert --save postfix >/dev/null 2>&1 ||:
%{_bindir}/systemd-sysv-convert --apply postfix >/dev/null 2>&1 ||:
/sbin/chkconfig --del postfix >/dev/null 2>&1 || :
/bin/systemctl try-restart postfix.service >/dev/null 2>&1 || :

%files
%defattr(-,root,root)
%config(noreplace) %{sasl_config_dir}/smtpd.conf
%config(noreplace) %{_sysconfdir}/pam.d/smtp.postfix
%config(noreplace) /etc/ld.so.conf.d/*
%{_unitdir}/postfix.service
%{postfix_doc_dir}
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/active
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/bounce
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/corrupt
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/defer
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/deferred
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/flush
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/hold
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/incoming
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/saved
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/trace
%dir %attr(0730,postfix,postdrop) %{postfix_queue_dir}/maildrop
%dir  %{postfix_queue_dir}/pid
%dir %attr(0700,postfix,root) %{postfix_queue_dir}/private
%dir %attr(0710,postfix,postdrop) %{postfix_queue_dir}/public
%dir %attr(0700,postfix,root) %{postfix_data_dir}

%{postfix_command_dir}/smtp-sink
%{postfix_command_dir}/smtp-source
%{postfix_command_dir}/postalias
%{postfix_command_dir}/postcat
%{postfix_command_dir}/postconf
%{postfix_command_dir}/postfix
%{postfix_command_dir}/postkick
%{postfix_command_dir}/postlock
%{postfix_command_dir}/postlog
%{postfix_command_dir}/postmap
%{postfix_command_dir}/postmulti
%{postfix_command_dir}/postsuper
%attr(2755,root,postdrop) %{postfix_command_dir}/postqueue
%attr(2755,root,postdrop) %{postfix_command_dir}/postdrop
%config(noreplace) %{postfix_config_dir}/access
%config(noreplace) %{postfix_config_dir}/canonical
%config(noreplace) %{postfix_config_dir}/generic
%config(noreplace) %{postfix_config_dir}/header_checks
%config(noreplace) %{postfix_config_dir}/relocated
%config(noreplace) %{postfix_config_dir}/transport
%config(noreplace) %{postfix_config_dir}/virtual
%config(noreplace) %{postfix_config_dir}/main.cf
%config(noreplace) %{postfix_config_dir}/master.cf
%config(noreplace) %{postfix_config_dir}/main.cf.proto
%config(noreplace) %{postfix_config_dir}/master.cf.proto
%{postfix_config_dir}/postfix-files
%{postfix_config_dir}/dynamicmaps.cf
%{postfix_config_dir}/dynamicmaps.cf.d/sqlite
%{postfix_config_dir}/postfix-files.d/sqlite
%{postfix_config_dir}/dynamicmaps.cf.d/ldap
%{postfix_config_dir}/postfix-files.d/ldap
%{postfix_config_dir}/dynamicmaps.cf.d/pcre
%{postfix_config_dir}/postfix-files.d/pcre
%{postfix_config_dir}/dynamicmaps.cf.d/mysql
%{postfix_config_dir}/postfix-files.d/mysql

%{postfix_shlib_dir}/postfix-pcre.so
%{postfix_shlib_dir}/postfix-ldap.so
%{postfix_shlib_dir}/postfix-mysql.so
%{postfix_shlib_dir}/postfix-sqlite.so
%{postfix_shlib_dir}/libpostfix-*.so
%{postfix_daemon_dir}/*
%{_bindir}/mailq.postfix
%{_bindir}/newaliases.postfix
%{_bindir}/rmail.postfix
%{_sbindir}/sendmail.postfix
/usr/lib/sendmail.postfix
%attr(0644, root, root) %{postfix_config_dir}/dynamicmaps.cf.d/lmdb
%attr(0644, root, root) %{postfix_config_dir}/postfix-files.d/lmdb
%attr(0755, root, root) %{postfix_shlib_dir}/postfix-lmdb.so
%ghost %{_sysconfdir}/pam.d/smtp
%ghost %attr(0755, root, root) %{_bindir}/mailq
%ghost %attr(0755, root, root) %{_bindir}/newaliases
%ghost %attr(0755, root, root) %{_bindir}/rmail
%ghost %attr(0755, root, root) %{_sbindir}/sendmail
%ghost %attr(0755, root, root) %{_prefix}/lib/sendmail
%ghost %attr(0644, root, root) %{_var}/lib/misc/postfix.aliasesdb-stamp
%exclude %{postfix_doc_dir}/pflogsumm-faq.txt
%exclude %{postfix_doc_dir}/README_FILES/*
%exclude %{_mandir}/man5/*_table.5*

%files sysvinit
%defattr(-,root,root)
%{_initrddir}/postfix

%files perl-scripts
%defattr(-,root,root)
%{postfix_command_dir}/qshape
%{postfix_command_dir}/pflogsumm

%files pgsql
%defattr(-,root,root)
%{postfix_config_dir}/dynamicmaps.cf.d/pgsql
%{postfix_config_dir}/postfix-files.d/pgsql
%{postfix_shlib_dir}/postfix-pgsql.so

%files help
%defattr(-,root,root)
%{_mandir}/man1/qshape*
%{_mandir}/man1/pflogsumm.1.gz
%{_mandir}/man1/post*.1*
%{_mandir}/man1/smtp*.1*
%{_mandir}/man1/*.postfix.1*
%{_mandir}/man5/access.5*
%{_mandir}/man5/[b-v]*.5*
%{_mandir}/man5/*.postfix.5*
%{_mandir}/man8/[a-qt-v]*.8*
%{_mandir}/man8/s[ch-lnp]*.8*
%{_mandir}/man8/smtp.8*
%{_mandir}/man8/smtpd.postfix.8*
%ghost %{_mandir}/man1/mailq.1.gz
%ghost %{_mandir}/man1/newaliases.1.gz
%ghost %{_mandir}/man5/aliases.5.gz
%ghost %{_mandir}/man8/sendmail.8.gz
%ghost %{_mandir}/man8/smtpd.8.gz
%doc %{postfix_doc_dir}/pflogsumm-faq.txt
%{postfix_doc_dir}/README_FILES/*

%changelog
* Tue Jan 02 2024 gaihuiying <eaglegai@163.com> - 2:3.8.4-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update postfix to 3.8.4

* Mon Aug 28 2023 gaihuiying <eaglegai@163.com> - 2:3.8.1-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:strip for smtp qmgr

* Tue Jul 25 2023 gaihuiying <eaglegai@163.com> - 2:3.8.1-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update postfix to 3.8.1

* Tue Jun 13 2023 zhouyihang <zhouyihang3@h-partners.com> - 2:3.7.4-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add require of help to fix empty symlink

* Sat Mar 11 2023 xinghe <xinghe2@h-partners.com> - 2:3.7.4-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC:update postfix to 3.7.4

* Fri Mar 10 2023 gaihuiying <eaglegai@163.com> - 2:3.7.2-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify Certificate validity period

* Fri Sep 30 2022 Funda Wang <fundawang@yeah.net> - 2:3.7.2-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:detect if hostname is executable before executing it

* Tue Sep 13 2022 xingwei <xingwei14@h-partners.com> - 2:3.7.2-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove runpath of files

* Mon Jul 04 2022 gaihuiying <eaglegai@163.com> - 2:3.7.2-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update postfix to 3.7.2
       switch from pcre to pcre2

* Thu Mar 03 2022 gaihuiying <eaglegai@163.com> - 2:3.3.1-19
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:set default_data_tpye to lmdb

* Tue Mar 01 2022 zhujunhao<zhujunhao11@huawei.com> - 2:3.3.1-18
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:set lmdb db in main.cf

* Tue Mar 01 2022 zhouyihang<zhouyihang3@h-partners.com> - 2:3.3.1-17
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add build require of lmdb

* Wed Feb 23 2022 xihaochen<xihaochen@h-partner.com> - 2:3.3.1-16
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove libdb dependency

* Tue Sep 07 2021 gaihuiying <gaihuiying1@huawei.com> - 2:3.3.1-15
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove runpath of files

* Tue Aug 10 2021 gaihuiying <gaihuiying1@huawei.com> - 2:3.3.1-14
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build error with glibc 2.34

* Fri Jul 30 2021 gaihuiying <gaihuiying1@huawei.com> - 2:3.3.1-13
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build error with gcc 10

* Fri Feb 05 2021 caodongxia <caodongxia@huawei.com> - 2:3.3.1-12
- Decouple the deprecated package tinycdb by removing subpackage postfix-cdb 

* Thu Jul 30 2020 gaihuiying <gaihuiying1@huawei.com> - 2:3.3.1-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build fail with glibc new version

* Wed Dec 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:3.3.1-10
- change permission of files

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:3.3.1-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix postfix build failed

* Tue Dec 3 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:3.3.1-8
- Type:NA
- ID:NA
- SUG:NA
- DESC:optimize the spec file

* Tue Oct 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:3.3.1-7
- Type:NA
- ID:NA
- SUG:NA
- DESC:Solve the problem of compilation failures

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:3.3.1-6
- Package init
